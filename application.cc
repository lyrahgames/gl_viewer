#include "application.h"
#include <iostream>
#include <stdexcept>

namespace Gl_viewer {

class Window {
 public:
  Window(int width, int height, const std::string& title) : window_(nullptr) {
    window_ = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr);
    if (!window_) throw std::runtime_error("GLFWwindow could not be created!");
    glfwMakeContextCurrent(window_);
  }
  ~Window() {
    glfwDestroyWindow(window_);
    std::cout << "Window destroyed!" << std::endl;
  }

  bool should_close() { return glfwWindowShouldClose(window_); }

  Window& swap_buffers() {
    glfwSwapBuffers(window_);
    return *this;
  }

  Window& make_context_current() {
    glfwMakeContextCurrent(window_);
    return *this;
  }

 private:
  GLFWwindow* window_;
};

Application::Application() : width_(640), height_(480), title_("Application") {
  if (!glfwInit()) throw std::runtime_error("GLFW could not be initialized!");
}

Application::~Application() {
  glfwTerminate();
  std::cout << "Application destroyed!" << std::endl;
}

Application& Application::resolution(int width, int height) {
  if (width > 0 && height > 0) {
    width_ = width;
    height_ = height;
  }
  return *this;
}

Application& Application::title(const std::string& title) {
  title_ = title;
  return *this;
}

Application& Application::execute() {
  Window window{width(), height(), title()};
  initialize();
  while (!window.should_close()) {
    render();
    window.swap_buffers();
    glfwPollEvents();
  }
  return *this;
}

}  // namespace Gl_viewer