#include <GL/glu.h>
#include <gl_viewer/application.h>
#include <Eigen/Dense>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>

void resize(GLFWwindow* window, int width, int height) {
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glViewport(0, 0, width, height);
  gluPerspective(45, static_cast<float>(width) / height, 0.1, 1000);
  glMatrixMode(GL_MODELVIEW);
}

class Viewer : public Gl_viewer::Application {
 public:
  void load(const std::string& file_path) {
    std::fstream file(file_path, std::ios::binary | std::ios::in);
    if (!file.is_open())
      throw std::runtime_error(std::string{"The file '"} + file_path +
                               "' could not be opened! Does it exist?");

    file.ignore(80);
    std::uint32_t primitive_count;
    file.read(reinterpret_cast<char*>(&primitive_count), 4);

    stl_data.resize(3 * primitive_count);

    for (auto i = 0; i < primitive_count; ++i) {
      file.ignore(12);
      file.read(reinterpret_cast<char*>(&stl_data[3 * i]), 36);
      file.ignore(2);
    }
  }

 private:
  void initialize() override {
    glClearColor(1.0, 1.0, 1.0, 1.0);
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0, 0, width(), height());
    gluPerspective(45, aspect_ratio(), 0.1, 1000);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(0, 0, 10, 0, 0, 0, 0, 1, 0);
  }

  void render() override {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glBegin(GL_TRIANGLES);
    glColor3f(0, 0, 0);
    for (const auto& vertex : stl_data) glVertex3fv(vertex.data());
    glEnd();
  }

  std::vector<Eigen::Vector3f> stl_data;
};

int main(int argc, char* argv[]) {
  if (2 != argc) {
    std::cout << "usage:" << std::endl
              << argv[0] << " <path to stl file>" << std::endl;
    return -1;
  }

  Viewer app;
  app.load(argv[1]);
  app.execute();
}