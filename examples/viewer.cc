#include <gl_viewer/application.h>

class Viewer : public Gl_viewer::Application {
  void initialize() override { glClearColor(1.0, 1.0, 1.0, 1.0); }
  void render() override { glClear(GL_COLOR_BUFFER_BIT); }
};

int main(int argc, char const *argv[]) {
  Viewer app;
  app.resolution(500, 500).title("Viewer");
  app.execute();
  app.resolution(100, 100).execute();
}