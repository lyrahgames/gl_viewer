add_executable(viewer viewer.cc)
target_link_libraries(viewer PRIVATE gl_viewer::gl_viewer)


find_package(Eigen3 REQUIRED)
find_package(OpenGL REQUIRED)
add_executable(stl_viewer stl_viewer.cc)
target_link_libraries(stl_viewer PRIVATE Eigen3::Eigen OpenGL::GLU gl_viewer::gl_viewer)