#ifndef GL_VIEWER_APPLICATION_H_
#define GL_VIEWER_APPLICATION_H_

#include <GLFW/glfw3.h>
#include <string>

namespace Gl_viewer {

class Application {
 public:
  Application();
  virtual ~Application();
  Application(const Application&) = delete;
  Application& operator=(const Application&) = delete;
  Application(Application&&) = delete;
  Application&& operator=(Application&&) = delete;

  Application& execute();

  Application& resolution(int width, int height);
  int width() const { return width_; }
  int height() const { return height_; }
  float aspect_ratio() const { return static_cast<float>(width()) / height(); }

  Application& title(const std::string& title);
  std::string& title() { return title_; }
  const std::string& title() const { return title_; }

 private:
  virtual void initialize() {}
  virtual void render() {}

  int width_;
  int height_;
  std::string title_;
};

}  // namespace Gl_viewer

#endif  // GL_VIEWER_APPLICATION_H_